(function ($) {
  Drupal.behaviors.saveSearchButton = {
    attach: function (context, settings) {
    	
    	function d_alert(data) {
    		if(Drupal.settings.save_search.sweetalert)
    		{
    			swal({  title: data.title, text: data.text, type: data.type, confirmButtonText: data.button_text });
    		}
    		else
    		{
    			alert(data.text);
    		}
    	}
    	
    	$(".delete_saved_search").on("click",function(){
    		$(this).after("<div class='loading_gif'></div>");
  		  $.post( "/save_search/delete", { id: $(this).attr("rel")})
      	  .done(function( data ) {
      		$("#save_search_button_load").hide();
      	    if(data.success)
      	    {
      	      var target = $("#record_"+data.id).parent().parent();
      	      target.hide('slow', function(){ target.remove(); });
      	    }
      	    else
      	    {
      	    	var message_data = {
  	    		    title: Drupal.t("Error"),
  	    		    text: data.message,
  	    		    type: Drupal.t("error"),
  	    		    button_text: Drupal.t("OK")
  	    		  }
  	    		  d_alert(message_data);
      	    }
      	  });
  	    });
    	
    	$(".search_notifications").change(function(event) {
    		var item = $(this);
    		  $(this).parent().append("<div class='loading_gif'></div>");
    		  var checked = ($(this).is(':checked')) ? 1 : 0;
    		  $.post( "/save_search/update_notification", { id: $(this).attr("rel"), notifications: checked})
          	  .done(function( data ) {
          		item.parent().find(".loading_gif").hide();
          	    if(data.success)
          	    {
          	      console.log(data.message);
          	    }
          	    else
          	    {
          	    	var message_data = {
      	    		    title: Drupal.t("Error"),
      	    		    text: data.message,
      	    		    type: Drupal.t("error"),
      	    		    button_text: Drupal.t("OK")
      	    		  }
      	    		  d_alert(message_data);
          	    }
          	  });
    	});
    	
    	$("#save_search_button").on("click",function(event) {
    	  var string_arr = Object.keys(Drupal.settings.urlIsAjaxTrusted);
    	  var search_query = string_arr[0];
    	  
    	  if(search_query.indexOf("?") == -1)
    	  {
    		  var data = {
    		    title: Drupal.t("Error"),
    		    text: Drupal.t("You need to refine your search in order to save"),
    		    type: Drupal.t("error"),
    		    button_text: Drupal.t("OK")
    		  }
    		  d_alert(data);
    		  return false;
    	  }
    	  
    	  $(this).addClass("passive");
    	  $("#save_search_button_load").show();
    	  
    	  var title = "";
    	  
		  var sPageURL = decodeURIComponent(window.location.search.substring(1)),
		  sURLVariables = sPageURL.split('&'),
		    sParameterName,
		    i;
		
			for (i = 0; i < sURLVariables.length; i++) {
			    sParameterName = sURLVariables[i].split('=');
			    sValueName = sParameterName[1].split(':');
			    title += sValueName[1] + ", ";
			}
			
			title = title.substr(0,title.length-2);
    	  
    	  $.post( "/save_search/save", { search_query: search_query, title: title })
    	  .done(function( data ) {
    		$("#save_search_button_load").hide();
    	    if(data.success)
    	    {
    	      $("#save_search_button_success").addClass("drawn");
    	      setTimeout('jQuery("#save_search_button_success").removeClass("drawn");',2000);
    	    }
    	    else
    	    {
    	    	$("#save_search_button_fail").addClass("drawn");
    	    	setTimeout('jQuery("#save_search_button_fail").removeClass("drawn");',2000);
    	    	
    	    	var message_data = {
	    		    title: Drupal.t("Error"),
	    		    text: data.message,
	    		    type: Drupal.t("error"),
	    		    button_text: Drupal.t("OK")
	    		  }
	    		  d_alert(message_data);
    	    }
    	    $("#save_search_button").removeClass("passive");
    	  });
    	  event.preventDefault();
    	});
    }
  };
})(jQuery);
