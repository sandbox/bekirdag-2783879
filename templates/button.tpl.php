<div id="save_search_button_wrapper">

  <input type="button" id="save_search_button" value="Save search" class="form-submit"/>
  
  <div id="save_search_button_load">
    <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" 
    viewBox="6 6 40 40" style="enable-background:new 0 0 50 50;" xml:space="preserve">
      <path fill="#ccc" d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z" transform="rotate(37.4594 25 25)">
        <animateTransform attributeType="xml" attributeName="transform" type="rotate" from="0 25 25" to="360 25 25" dur="0.6s" repeatCount="indefinite"></animateTransform>
      </path>
    </svg>
  </div>
  
  
  <div id="save_search_result">
  
    <!-- success svg start -->
    <div class="svg_wrapper success">
      <div id="save_search_button_success" class="success_animation"></div>
      <svg version="1.1" id="tick" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
  	 viewBox="0 0 37 37" style="enable-background:new 0 0 37 37;" xml:space="preserve">
        <path class="circ path" style="fill:none;stroke:#1CA739;stroke-width:3;stroke-linejoin:round;stroke-miterlimit:10;" d="
  	M30.5,6.5L30.5,6.5c6.6,6.6,6.6,17.4,0,24l0,0c-6.6,6.6-17.4,6.6-24,0l0,0c-6.6-6.6-6.6-17.4,0-24l0,0C13.1-0.2,23.9-0.2,30.5,6.5z"
  	/>
        <polyline class="tick path" style="fill:none;stroke:#1CA739;stroke-width:3;stroke-linejoin:round;stroke-miterlimit:10;" points="
  	11.6,20 15.9,24.2 26.4,13.8 "/>
      </svg>
    </div>
    <!-- success svg end -->
    
    <!-- fail svg start -->
    <div class="svg_wrapper fail">
      <div id="save_search_button_fail" class="fail_animation"></div>
      <svg version="1.1" id="save_search_error" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 37 37" style="enable-background:new 0 0 37 37;" xml:space="preserve">
        <path class="circ path red" style="fill:none;stroke:#bd0000;stroke-width:3;stroke-linejoin:round;stroke-miterlimit:10;" d="
  	M30.5,6.5L30.5,6.5c6.6,6.6,6.6,17.4,0,24l0,0c-6.6,6.6-17.4,6.6-24,0l0,0c-6.6-6.6-6.6-17.4,0-24l0,0C13.1-0.2,23.9-0.2,30.5,6.5z"></path>
        <polyline class="tick path" style="fill:none;stroke:#bd0000;stroke-width:3;stroke-linejoin:round;stroke-miterlimit:10;" points="  30 7, 7 30"></polyline>
        <polyline class="tick path" style="fill:none;stroke:#bd0000;stroke-width:3;stroke-linejoin:round;stroke-miterlimit:10;" points="  7 7, 30 30"></polyline>
      </svg>
    </div>
    <!-- fail svg end -->
    
   </div>
  

  
  <div class="clear_both"></div>

</div>